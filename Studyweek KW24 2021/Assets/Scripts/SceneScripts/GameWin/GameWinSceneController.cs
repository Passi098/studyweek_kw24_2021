using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWinSceneController : MonoBehaviour
{
    public void RestartGame()
    {
        GameSceneManagement.SwitchScene(GameSceneManagement.EGameScenes.MainGame);
    }

    public void GoToMainMenu()
    {
        GameSceneManagement.SwitchScene(GameSceneManagement.EGameScenes.MainMenu);
    }
}
