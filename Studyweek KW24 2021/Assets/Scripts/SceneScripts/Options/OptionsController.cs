using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour
{
    [SerializeField]
    private Slider m_MasterVolume;

    [SerializeField]
    private Slider m_MusicVolume;

    [SerializeField]
    private Slider m_EffectVolume;

    private void Awake()
    {
        m_MasterVolume.value = (int)AudioManager.GetVolume(AudioManager.EAudioChannels.Master);
        m_MusicVolume.value  = (int)AudioManager.GetVolume(AudioManager.EAudioChannels.Music);
        m_EffectVolume.value = (int)AudioManager.GetVolume(AudioManager.EAudioChannels.Effects);
        ChangeMasterVolume();
        ChangeMusicVolume();
        ChangeEffectVolume();
    }

    public void ChangeMasterVolume()
    {
        AudioManager.SetVolume(AudioManager.EAudioChannels.Master, m_MasterVolume.value);
    }

    public void ChangeMusicVolume()
    {
        AudioManager.SetVolume(AudioManager.EAudioChannels.Music, m_MusicVolume.value);
    }

    public void ChangeEffectVolume()
    {
        AudioManager.SetVolume(AudioManager.EAudioChannels.Effects, m_EffectVolume.value);
    }

    public void BackToMainMenu()
    {
        GameSceneManagement.SwitchScene(GameSceneManagement.EGameScenes.MainMenu);
    }
}
