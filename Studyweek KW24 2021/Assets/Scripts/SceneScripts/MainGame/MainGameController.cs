using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameController : MonoBehaviour
{
    private static MainGameController m_Instance;

    private const float MULTI_KILL_TIME_RANGE = 3f;
    private int m_MultiKillCounter = 0;

    [SerializeField]
    private AudioClip m_ClockTick1;
    [SerializeField]
    private AudioClip m_ClockTick2;
    [SerializeField]
    private AudioClip m_TimeEnd;

    private static int m_CurrentSecond;
    private static bool m_ClockTick = true;

    [SerializeField]
    private AudioSource m_ClockTicking;

    private void Awake()
    {
        if(m_Instance != null)
        {
            return;
        }
        m_Instance = this;
        m_CurrentSecond = (int)GameRegistry.Timer;
    }

    private void OnDestroy()
    {
        if(m_Instance == this)
        {
            m_Instance = null;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Timer.FixedUpdate();
    }

    private static class Timer
    {
        public static void FixedUpdate()
        {
            if(GameRegistry.m_TimeFreez)
            {
                return;
            }
            GameRegistry.Timer -= Time.fixedDeltaTime;
            if((int)GameRegistry.Timer < m_CurrentSecond)
            {
                m_CurrentSecond = (int)GameRegistry.Timer;
                if (GameRegistry.Timer <= 0)
                {
                    m_Instance.m_ClockTicking.clip = m_Instance.m_TimeEnd;
                }
                else
                {
                    if (m_ClockTick)
                    {
                        m_Instance.m_ClockTicking.clip = m_Instance.m_ClockTick1;
                    }
                    else
                    {
                        m_Instance.m_ClockTicking.clip = m_Instance.m_ClockTick2;
                    }
                    m_Instance.m_ClockTicking.Play();
                    m_ClockTick = !m_ClockTick;
                }
            }
        }
    }
}
