using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainGameUI : MonoBehaviour
{
    private static MainGameUI m_Instance;

    [SerializeField]
    private Text m_TimerText;

    private void Awake()
    {
        if(m_Instance != null)
        {
            Destroy(this);
            return;
        }
        m_Instance = this;
    }

    public static void UpdateTimerUI()
    {
        m_Instance.m_TimerText.text = ((int)GameRegistry.Timer + 1).ToString();
    }
}
