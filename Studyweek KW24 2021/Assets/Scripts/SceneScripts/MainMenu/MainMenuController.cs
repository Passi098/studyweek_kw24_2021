using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    public void GoToOptions()
    {
        GameSceneManagement.SwitchScene(GameSceneManagement.EGameScenes.Options);
    }

    public void StartGame()
    {
        GameSceneManagement.SwitchScene(GameSceneManagement.EGameScenes.MainGame);
    }

    public void GoToCredits()
    {
        GameSceneManagement.SwitchScene(GameSceneManagement.EGameScenes.Credits);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
