using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private const float NOCKBACK = 0.1f;
    private const float LARGE_NOCKBACK = 4f;
    private const float ENEMY_MAX_HEALTH = 3f;
    private float enemyCurrentHealth;

    private Animator enemyAnimator;
    private Rigidbody2D rb;

    private void Awake()
    {
        enemyCurrentHealth = ENEMY_MAX_HEALTH;
        rb = GetComponent<Rigidbody2D>();
        enemyAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (enemyCurrentHealth <= 0)
        {
            enemyAnimator.Play("Death");
            StartCoroutine(DyingDelay());
        }

    }

    public void GetDamage(float _damage)
    {
        enemyCurrentHealth -= _damage;
        enemyAnimator.Play("Hurt");
        Debug.Log(gameObject + " Health = " + enemyCurrentHealth);
        if (enemyCurrentHealth <= 0)
        {
            Die();
        }
        else
        {
            StartCoroutine(DamageDelay());
            NockBack();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<Player>() != null)
        {
            switch (Player.PlayerState)
            {
                case Player.EPlayerState.DashEntry:
                case Player.EPlayerState.Dash:
                case Player.EPlayerState.DashExit:
                case Player.EPlayerState.Attack1:
                case Player.EPlayerState.Attack2:
                case Player.EPlayerState.Attack3:
                case Player.EPlayerState.Hurt:
                    {
                        return;
                    }
            }

            enemyAnimator.Play("Attack");
            Player.GetDamage();
        }
    }

    private void Die()
    {
        Collider2D[] colliders = GetComponents<Collider2D>();
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].enabled = false;
        }
        Player.RemoveEmidiatlyEnemyFromList(this);
        GetComponent<Collider2D>().enabled = false;
        StartCoroutine(DyingDelay());
        LargeNockback();
    }

    public void NockBack()
    {
        transform.position += transform.right * NOCKBACK;
    }

    public void LargeNockback()
    {
        transform.position += transform.right * LARGE_NOCKBACK;
    }

    public void InAttackRange()
    {
        enemyAnimator.Play("Attack");
    }

    public void EnemyHitBack()
    {
        transform.position = new Vector2(transform.position.x + 7, transform.position.y);
    }

    IEnumerator DamageDelay()
    {
        enemyAnimator.SetBool("isGettingDamage", true);
        yield return new WaitForSeconds(0.2f);
        enemyAnimator.SetBool("isGettingDamage", false);
    }

    IEnumerator DyingDelay()
    {
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
