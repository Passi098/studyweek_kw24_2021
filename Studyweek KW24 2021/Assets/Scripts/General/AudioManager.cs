﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour //Done by: Pascal
{
    public enum EAudioChannels { Master, Music, Effects }

    private static AudioManager m_Instance;

    [SerializeField]
    private AudioSource m_MusicPlayer;

    [SerializeField]
    private AudioClip[] m_MusicPlaylist;

    [SerializeField]
    private AudioMixer m_AudioMixer;

    [SerializeField]
    private static bool m_PlayMusic = true;

    private static int m_SongIndex = 0;

    private static AudioManager Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = new GameObject().AddComponent<AudioManager>();
            }
            return m_Instance;
        }
    }

    private static AudioMixer AudioMixer
    {
        get { return m_Instance.m_AudioMixer; }
    }

    public static bool Ready
    {
        get
        {
            return MusicReady;
        }
    }


    void Awake()
    {
        Debug.Log("AudioManager: Instantiation.");
        if (m_Instance == null)
        {
            Debug.Log("AudioManager: Instantiated.");
            m_Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Debug.LogWarning("AudioManager: Allready exists.");
            Destroy(gameObject);
        }

    }

    #region Channels

    public static void SetVolume(EAudioChannels _channel, float _value)
    {
        Mathf.Clamp(_value, 0, 100);
        _value -= 80;
        switch (_channel)
        {
            case EAudioChannels.Master:
                {
                    AudioMixer.SetFloat("MasterVolume", _value);
                }break;
            case EAudioChannels.Music:
                {
                    AudioMixer.SetFloat("MusicVolume", _value);
                }
                break;
            case EAudioChannels.Effects:
                {
                    AudioMixer.SetFloat("EffectVolume", _value);
                }
                break;
        }
    }
    
    public static float GetVolume(EAudioChannels _channel)
    {
        float value = 0;
        switch (_channel)
        {
            case EAudioChannels.Master:
                {
                    AudioMixer.GetFloat("MasterVolume", out value);
                }break;
            case EAudioChannels.Music:
                {
                    AudioMixer.GetFloat("MusicVolume", out value);
                }break;
            case EAudioChannels.Effects:
                {
                    AudioMixer.GetFloat("EffectVolume", out value);
                }break;
        }
        return value + 80;
    }

    #endregion Channels

    #region Music
    public static bool MusicReady
    {
        get
        {
            if (Instance != null)
            {
                return Instance.m_MusicPlayer != null;
            }
            return false;
        }
    }

    /// <summary>
    /// Command Musicplayer to play/continue actuelle Song 
    /// </summary>
    public static void MusicPlay()
    {
        m_PlayMusic = true;
        if (GetVolume(EAudioChannels.Music) == 0)
        {
            Debug.LogWarning("AudioManager: Cant play music. Volume is set to 0.");
            return;
        }
        Instance.m_MusicPlayer.Play();
    }


    /// <summary>
    /// Command Musicplayer to stop playing Music
    /// </summary>
    public static void MusicStop()
    {
        m_PlayMusic = false;
        Instance.m_MusicPlayer.Stop();
    }


    /// <summary>
    /// Command Musicplayer to stop playing the actuelle Song if he playes one and play the next Song in the MusicPlaylist
    /// </summary>
    public static void MusicNext()
    {
        if (Instance.m_MusicPlaylist.Length == 0)
        {
            Debug.LogWarning("AudioManager: No song in playlist.");
            return;
        }
        MusicStop();
        m_SongIndex++;
        if (m_SongIndex >= Instance.m_MusicPlaylist.Length)
        {
            m_SongIndex = 0;
        }
        Instance.m_MusicPlayer.clip = Instance.m_MusicPlaylist[m_SongIndex];
        MusicPlay();
    }

    /// <summary>
    /// Plays a song by index
    /// </summary>
    /// <param name="Index"></param>
    public static void PlaySong(int _index)
    {
        if (_index >= Instance.m_MusicPlaylist.Length)
        {
            Debug.LogError("AudioManager: Index out of Range.");
            return;
        }
        m_SongIndex = _index;
        MusicStop();
        Instance.m_MusicPlayer.clip = Instance.m_MusicPlaylist[m_SongIndex];
        MusicPlay();
    }


    /// <summary>
    /// Controlls the Volume of the Music on a Scale 0 (-80dB) - 100(+20dB).
    /// If Volume is 0 the Musicplayer stop playing Music.
    /// </summary>
    public static float MusicVolume
    {
        get
        {
            Instance.m_AudioMixer.GetFloat("MusicVolume", out float volume);
            if (!Instance.m_MusicPlayer.isPlaying && volume == -80)
            {
                return 0;
            }
            else
            {
                // Convert the Value (-80dB/ +20dB) to a 0 - 100 Scale
                Instance.m_AudioMixer.GetFloat("MusicVolume", out volume);
                return 80 + volume;
            }
        }

        set
        {
            if (value > 100 || value < 0)
            {
                Debug.LogError("AudioManager: Volume out of range.");
                return;
            }
            if (value == 0)
            {
                MusicStop();
                Instance.m_AudioMixer.SetFloat("MusicVolume", -80);
                return;
            }
            if (!Instance.m_MusicPlayer.isPlaying)
            {
                MusicNext();
            }
            Instance.m_AudioMixer.SetFloat("MusicVolume", value - 80);
        }
    }


    /// <summary>
    /// Get : Is Music looping.
    /// Set :  true = Music loops.
    ///       false = Music does not loop.
    /// </summary>
    public static bool MusicLooping
    {
        get { return Instance.m_MusicPlayer.loop; }
        set { Instance.m_MusicPlayer.loop = value; }
    }


    /// <summary>
    /// Get : Is Music muted.
    /// Set:  true: Music is muted
    ///      false: Music is unmuted
    /// </summary>
    public static bool MusicMuted
    {
        get { return Instance.m_MusicPlayer.mute; }
        set { Instance.m_MusicPlayer.mute = value; }
    }

    /// <summary>
    /// Switch between Looping and not Looping
    /// </summary>
    public static void MusicLoop()
    {
        Instance.m_MusicPlayer.loop = !Instance.m_MusicPlayer.loop;
    }

    /// <summary>
    /// Switch between muted/Unmuted
    /// </summary>
    public static void MusicMute()
    {

        Instance.m_MusicPlayer.mute = !Instance.m_MusicPlayer.mute;
    }

    /// <summary>
    /// Plays a random Song from the Playlist
    /// </summary>
    public static void MusicRandomSong()
    {
        if (Instance.m_MusicPlaylist.Length == 0)
        {
            Debug.LogWarning("Audiomanager: No song in Playlist");
            return;
        }
        MusicStop();
        m_SongIndex = Random.Range(0, Instance.m_MusicPlaylist.Length);
        Instance.m_MusicPlayer.clip = Instance.m_MusicPlaylist[m_SongIndex];
        MusicPlay();
    }

    /// <summary>
    /// New Playlist get the MusicPlaylist and it plays a random Song from it
    /// </summary>
    /// <param name="New Playlist"></param>
    public static void MusicLoadPlaylist(AudioClip[] _playlist)
    {
        if (_playlist == null)
        {
            Debug.LogError("AudioManager: New playlist empty");
            return;
        }
        Instance.m_MusicPlaylist = _playlist;
        MusicRandomSong();
    }
    #endregion Music

    /// <summary>
    /// If actuelle Song is over play the next
    /// </summary>
    void Update()
    {
        if (Instance == null)
        {
            return;
        }
        if (Instance.m_MusicPlayer.clip == null)
        {
            MusicRandomSong();
        }
        if (!Instance.m_MusicPlayer.isPlaying && m_PlayMusic)
        {
            MusicNext();
        }
    }

    private void OnDestroy()
    {
        Debug.LogWarning("AudioManager got destroyed");
    }
}
