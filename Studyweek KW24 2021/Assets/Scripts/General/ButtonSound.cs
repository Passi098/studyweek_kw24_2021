using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(EventTrigger))]
public class ButtonSound : MonoBehaviour
{
    private AudioSource m_AudioSource;

    [SerializeField]
    AudioClip m_MouseOver;

    [SerializeField]
    AudioClip m_MouseDown;

    private void Awake()
    {
        m_AudioSource = GetComponent<AudioSource>();
    }

    public void MouseEnter()
    {
        m_AudioSource.Stop();
        m_AudioSource.clip = m_MouseOver;
        m_AudioSource.Play();
    }

    public void MouseDown()
    {
        m_AudioSource.Stop();
        m_AudioSource.clip = m_MouseDown;
        m_AudioSource.Play();
    }
}
