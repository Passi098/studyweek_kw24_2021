using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class KeyBindings
{
    public static KeyCode m_JumpKey = KeyCode.Space;
    public static KeyCode m_SlideKey = KeyCode.LeftControl;
    public static KeyCode m_DashKey = KeyCode.LeftShift;
    public static KeyCode m_AttackKey = KeyCode.Mouse0;
    public static KeyCode m_DashUp = KeyCode.W;
    public static KeyCode m_DashDown = KeyCode.S;
    public static KeyCode m_DashLeft = KeyCode.A;
    public static KeyCode m_DashRight = KeyCode.D;
}
