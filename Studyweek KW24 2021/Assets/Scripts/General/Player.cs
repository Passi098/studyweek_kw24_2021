using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class Player : MonoBehaviour
{
    private static Player m_PlayerInstance;

    public static bool Attackable = true;

    public enum EPlayerState { Run, Jump, Fall, Slide,DashEntry, Dash, DashExit, Attack1, Attack2, Attack3, Hurt, Die}

    #region Constants


    private const float PLAYER_HURT_TIME = 1.5f;
    private const float PLAYER_KNOCKBACK = 10f;
    private const float DEATH_DELAY = 1f;
    private const float FINISH_LINE = 320f;
    private const float JUMP_STRENGTH = 10f;
    private const float PLAYER_SPEED = 10f;
    private const float PLAYER_ACCELERATION = 1000f;
    private const float PLAYER_DASH_SPEED = 15f;
    private const float PLAYER_DASH_COOLDOWN = 10f;
    private const int PLAYER_ATTACK_AMOUNT = 3;
    private const float PLAYER_ATTACK_TIME_RESET_AMOUNT = PLAYER_DASH_COOLDOWN / PLAYER_ATTACK_AMOUNT;
    private const float PLAYER_ATTACK_COOLDOWN = 1f;
    private const float PLAYER_ATTACK_COOLDOWN_DELAY = 0.5f;
    private const float PLAYER_DASH_DURATION = 0.1f;
    private const float PLAYER_DASH_ENTRY = 0.5f;
    private const float PLAYER_DASH_EXIT = 0.5f;

    #endregion Constants

    #region Variables

    [SerializeField]
    private AudioClip m_RunSound;

    [SerializeField]
    private AudioClip m_JumpSound;

    [SerializeField]
    private AudioClip m_SlideSound;

    [SerializeField]
    private AudioClip m_DashEntrySound;

    [SerializeField]
    private AudioClip m_DashSound;

    [SerializeField]
    private AudioClip m_DashExitSound;

    [SerializeField]
    private AudioClip m_Attack1Sound;

    [SerializeField]
    private AudioClip m_Attack2Sound;

    [SerializeField]
    private AudioClip m_Attack3Sound;

    [SerializeField]
    private AudioClip m_HurtSound;

    [SerializeField]
    private AudioClip m_DieSound;

    private IEnumerator<bool> m_CurrentBehaviour;

    private EPlayerState m_CurrentPlayerState;
    private EPlayerState m_NextPlayerState;

    Vector2 m_VelocityChanger = new Vector2(PLAYER_SPEED, 0);
    Vector3 m_DashDirection = Vector3.right;

    protected int m_AttackStack = 0;
    protected float m_AttackCoolDown = 0;
    protected float m_AttackCooldownDelay = 0;
    private Rigidbody2D m_RB;
    private Animator m_Animator;
    private BoxCollider2D m_Collider;
    private AudioSource m_AudioSource;

    private float m_DashCooldown = 0;
    private bool m_GotDashReset = true;

    private float m_LeftDashTime = PLAYER_DASH_DURATION;
    private float m_DashEntryTime = PLAYER_DASH_ENTRY;
    private float m_DashExitTime = PLAYER_DASH_EXIT;

    private bool m_GotJumpReset = true;

    private bool m_OnGround = true;
    private bool m_ChangeState = false;

    private bool m_JumpKeyDown = false;
    private bool m_SlideKeyDown = false;
    private bool m_DashKeyDown = false;
    private bool m_AttackKeyDown = false;

    private List<Enemy> m_InRange = new List<Enemy>();
    private List<Enemy> m_Remove = new List<Enemy>();

    #endregion Variables

    #region Properties

    public static Vector3 PlayerPosition
    {
        get { return m_PlayerInstance.transform.position; }
    }

    protected bool DashReady
    {
        get { return (m_DashCooldown <= 0 && m_GotDashReset); }
    }

    protected virtual string CharacterName
    {
        get { return "Anubis"; }
    }

    public static EPlayerState PlayerState
    {
        get { return m_PlayerInstance.m_CurrentPlayerState; }
    }

    #endregion Properties

    #region Unity Methodes

    private void Awake()
    {
        PlayerAwake();
    }

    private void FixedUpdate()
    {
        PlayerFixedUpdate();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Ground"))
        {
            m_OnGround = true;
            m_GotJumpReset = true;
            m_GotDashReset = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            m_OnGround = false;
        }
    }

    private void Update()
    {
        PlayerUpdate();
        switch (m_CurrentPlayerState)
        {
            case EPlayerState.Attack1:
            case EPlayerState.Attack2:
            case EPlayerState.Attack3:
                {

                }
                break;
            default:
                {
                    for (int i = 0; i < m_Remove.Count; i++)
                    {
                        m_InRange.Remove(m_Remove[i]);
                    }
                    m_Remove.Clear();
                }
                break;
        }
        m_JumpKeyDown = Input.GetKeyDown(KeyBindings.m_JumpKey);
        m_SlideKeyDown = Input.GetKeyDown(KeyBindings.m_SlideKey);
        m_DashKeyDown = Input.GetKeyDown(KeyBindings.m_DashKey);
        m_AttackKeyDown = Input.GetKeyDown(KeyBindings.m_AttackKey);
    }

    #endregion Unity Methodes

    #region StateBehaviour

    private IEnumerator<bool> RunBehaviour()
    {
        m_ChangeState = false;
        ChangeAnimation(EPlayerState.Run);
        m_CurrentPlayerState = EPlayerState.Run;
        m_RB.simulated = true;
        PlaySound();

        do
        {
            yield return false;

            HoldSpeed();
            if (m_RB.velocity.y < 0)
            {
                ChangeState(EPlayerState.Fall);
            }
            else if (Input.GetKeyDown(KeyBindings.m_JumpKey))
            {
                ChangeState(EPlayerState.Jump);
            }
            else if(Input.GetKeyDown(KeyBindings.m_SlideKey))
            {
                ChangeState(EPlayerState.Slide);
            }
            else if(Input.GetKeyDown(KeyBindings.m_DashKey))
            {
                ChangeState(EPlayerState.DashEntry);
            }
            else if(Input.GetKeyDown(KeyBindings.m_AttackKey))
            {
                ChangeState(EPlayerState.Attack1);
            }
        } while (!m_ChangeState);
        yield return true;
    }

    private IEnumerator<bool> JumpBehaviour()
    {
        m_ChangeState = false;
        m_GotJumpReset = false;
        ChangeAnimation(EPlayerState.Jump);
        m_CurrentPlayerState = EPlayerState.Jump;
        m_RB.simulated = true;
        m_RB.AddForce(Vector2.up * JUMP_STRENGTH, ForceMode2D.Impulse);

        do
        {
            yield return false;

            HoldSpeed();
            if(m_RB.velocity.y < 0)
            {
                ChangeState(EPlayerState.Fall);
            }
            else if (Input.GetKeyDown(KeyBindings.m_DashKey))
            {
                ChangeState(EPlayerState.DashEntry);
            }
            else if (Input.GetKeyDown(KeyBindings.m_AttackKey))
            {
                ChangeState(EPlayerState.Attack1);
            }
            
        } while (!m_ChangeState);
        yield return true;
    }

    private IEnumerator<bool> FallBehaviour()
    {
        m_ChangeState = false;
        ChangeAnimation(EPlayerState.Fall);
        m_CurrentPlayerState = EPlayerState.Fall;

        do
        {
            yield return false;

            HoldSpeed();
            if (Input.GetKeyDown(KeyBindings.m_AttackKey))
            {
                ChangeState(EPlayerState.Attack1);
            }
            else if (Input.GetKeyDown(KeyBindings.m_JumpKey))
            {
                ChangeState(EPlayerState.Jump);
            }
            else if (Input.GetKeyDown(KeyBindings.m_DashKey))
            {
                ChangeState(EPlayerState.DashEntry);
            }
            else if(m_RB.velocity.y >= 0)
            {
                ChangeState(EPlayerState.Run);
            }
        } while (!m_ChangeState);
        yield return true;
    }

    private IEnumerator<bool> SlideBehaviour()
    {
        m_ChangeState = false;
        m_Collider.size = new Vector2(m_Collider.size.x, m_Collider.size.y / 2);
        m_Collider.offset = new Vector2(-0.1f, -0.1f) - Vector2.up * m_Collider.size.y / 2;
        ChangeAnimation(EPlayerState.Slide);
        m_CurrentPlayerState = EPlayerState.Slide;

        do
        {
            yield return false;

            HoldSpeed();
            if (Input.GetKeyDown(KeyBindings.m_AttackKey))
            {
                ChangeState(EPlayerState.Attack1);
            }
            if(!Input.GetKey(KeyBindings.m_SlideKey))
            {
                ChangeState(EPlayerState.Run);
            }
        } while (!m_ChangeState);
        m_Collider.size = new Vector2(m_Collider.size.x, m_Collider.size.y * 2);
        m_Collider.offset = new Vector2(-0.1f, -0.1f);
        yield return true;
    }

    private IEnumerator<bool> DashBehaviour()
    {
        m_ChangeState = false;
        m_LeftDashTime = PLAYER_DASH_DURATION;
        ChangeAnimation(EPlayerState.Dash);
        m_CurrentPlayerState = EPlayerState.Dash;
        m_RB.simulated = false;

        do
        {
            yield return false;

            if (!Input.GetKey(KeyBindings.m_DashKey) || m_LeftDashTime <= 0)
            {
                ChangeState(EPlayerState.DashExit);
            }
        } while (!m_ChangeState);
        yield return true;
    }

    private IEnumerator<bool> DashEntryBehaviour()
    {
        m_ChangeState = false;
        m_DashEntryTime = PLAYER_DASH_ENTRY;
        m_AttackCoolDown = 0f;
        ChangeAnimation(EPlayerState.DashEntry);
        m_CurrentPlayerState = EPlayerState.DashEntry;
        m_RB.simulated = false;
        m_DashDirection = Vector3.right;

        //if (Input.GetKey(KeyBindings.m_DashUp))
        //{
        //    m_DashDirection.y += 1;
        //}

        //m_DashDirection.Normalize();

        do
        {
            yield return false;
            if(m_DashEntryTime <= 0)
            {
                ChangeState(EPlayerState.Dash);
            }
        } while (!m_ChangeState);
        yield return true;
    }

    private IEnumerator<bool> DashExitBehaviour()
    {
        m_ChangeState = false;
        m_DashExitTime = PLAYER_DASH_EXIT;
        m_DashCooldown = PLAYER_DASH_COOLDOWN;
        m_GotDashReset = false;
        ChangeAnimation(EPlayerState.DashExit);
        m_CurrentPlayerState = EPlayerState.DashExit;
        m_RB.simulated = false;

        do
        {
            yield return false;
            if(Input.GetKeyDown(KeyBindings.m_AttackKey))
            {
                ChangeState(EPlayerState.Attack1);
            }
            else if (m_DashExitTime <= 0)
            {
                ChangeState(EPlayerState.Run);
            }
        } while (!m_ChangeState);
        yield return true;
    }

    private IEnumerator<bool> Attack1Behaviour()
    {
        m_ChangeState = false;
        m_AttackCooldownDelay = PLAYER_ATTACK_COOLDOWN_DELAY;
        ChangeAnimation(EPlayerState.Attack1);
        m_CurrentPlayerState = EPlayerState.Attack1;
        m_RB.simulated = false;
        if(!Attack())
        {
            do
            {
                yield return false;
                if (m_AttackCooldownDelay <= 0)
                {
                    m_AttackCoolDown = PLAYER_ATTACK_COOLDOWN;
                    GameRegistry.m_TimeFreez = false;
                    ChangeState(EPlayerState.Run);
                }
            } while (!m_ChangeState);
        }
        else
        {
            m_GotJumpReset = true;
            m_GotDashReset = true;
            do
            {
                yield return false;
                if (Input.GetKeyDown(KeyBindings.m_AttackKey))
                {
                    ChangeState(EPlayerState.Attack2);
                }
                else if(Input.GetKeyDown(KeyBindings.m_DashKey))
                {
                    NockAllBack();
                    GameRegistry.m_TimeFreez = false;
                    ChangeState(EPlayerState.DashEntry);
                }
                else if(m_AttackCooldownDelay <= 0)
                {
                    NockAllBack();
                    GameRegistry.m_TimeFreez = false;
                    m_AttackCoolDown = PLAYER_ATTACK_COOLDOWN;
                    ChangeState(EPlayerState.Run);
                }
            } while (!m_ChangeState);
        }

        
        yield return true;
    }

    private IEnumerator<bool> Attack2Behaviour()
    {
        m_ChangeState = false;
        m_AttackCooldownDelay = PLAYER_ATTACK_COOLDOWN_DELAY;
        ChangeAnimation(EPlayerState.Attack2);
        m_CurrentPlayerState = EPlayerState.Attack2;
        if (!Attack())
        {
            do
            {
                yield return false;
                if (m_AttackCooldownDelay <= 0)
                {
                    m_AttackCoolDown = PLAYER_ATTACK_COOLDOWN;
                    GameRegistry.m_TimeFreez = false;
                    ChangeState(EPlayerState.Run);
                }
            } while (!m_ChangeState);
        }
        else
        {

            do
            {
                yield return false;
                if (Input.GetKeyDown(KeyBindings.m_AttackKey))
                {
                    ChangeState(EPlayerState.Attack3);
                }
                else if (Input.GetKeyDown(KeyBindings.m_DashKey))
                {
                    NockAllBack();
                    GameRegistry.m_TimeFreez = false;
                    ChangeState(EPlayerState.DashEntry);
                }
                else if (m_AttackCooldownDelay <= 0)
                {
                    NockAllBack();
                    m_AttackCoolDown = PLAYER_ATTACK_COOLDOWN;
                    GameRegistry.m_TimeFreez = false;
                    ChangeState(EPlayerState.Run);
                }
            } while (!m_ChangeState);
        }
        yield return true;
    }

    private IEnumerator<bool> Attack3Behaviour()
    {
        m_ChangeState = false;
        m_AttackCooldownDelay = PLAYER_ATTACK_COOLDOWN_DELAY;
        m_AttackCoolDown = PLAYER_ATTACK_COOLDOWN;
        ChangeAnimation(EPlayerState.Attack3);
        m_CurrentPlayerState = EPlayerState.Attack3;
        if (Attack())
        {
            DashFullReset();
            do
            {
                yield return false;
                if (Input.GetKeyDown(KeyBindings.m_DashKey))
                {
                    NockAllBack();
                    GameRegistry.m_TimeFreez = false;
                    ChangeState(EPlayerState.DashEntry);
                }
                else if (m_AttackCooldownDelay <= 0)
                {
                    NockAllBack();
                    m_AttackCoolDown = PLAYER_ATTACK_COOLDOWN;
                    GameRegistry.m_TimeFreez = false;
                    ChangeState(EPlayerState.Run);
                }
            } while (!m_ChangeState);
        }
        else
        {
            do
            {
                yield return false;
                if (m_AttackCooldownDelay <= 0)
                {
                    m_AttackCoolDown = PLAYER_ATTACK_COOLDOWN;
                    GameRegistry.m_TimeFreez = false;
                    ChangeState(EPlayerState.Run);
                }
            } while (!m_ChangeState);

        }

        
        yield return true;
    }

    private IEnumerator<bool> DieBehaviour()
    {
        while (true)
        {
            yield return false;
        }
    }

    private IEnumerator<bool> HurtBehaviour()
    {
        m_ChangeState = false;
        ChangeAnimation(EPlayerState.Hurt);
        m_CurrentPlayerState = EPlayerState.Hurt;
        m_RB.simulated = true;
        m_RB.velocity = (-Vector2.right + Vector2.up).normalized * PLAYER_KNOCKBACK;
        float time = PLAYER_HURT_TIME;
        while (time >= 0)
        {
            time -= Time.fixedDeltaTime;
            yield return false;
        }
        Attackable = true;
        ChangeState(EPlayerState.Run);
        yield return true;
    }

    #endregion StateBehaviour

    #region Methodes

    public static void GetDamage()
    {
        
        GameRegistry.PlayerHealth--;
        if(GameRegistry.PlayerHealth <= 0) 
        {
            m_PlayerInstance.ChangeAnimation(EPlayerState.Die);
            m_PlayerInstance.StartCoroutine(m_PlayerInstance.DeathDelay());
            m_PlayerInstance.ChangeState(EPlayerState.Die);
            Attackable = false;
        }
        else
        {
            m_PlayerInstance.ChangeState(EPlayerState.Hurt);
            Attackable = false;
        }
    }

    private IEnumerator DeathDelay()
    {
        yield return new WaitForSeconds(DEATH_DELAY);
        GameSceneManagement.SwitchScene(GameSceneManagement.EGameScenes.GameLost);
    }

    public static void AddEnemyToList(Enemy _enemy)
    {
        Debug.Log("Greif an");
        m_PlayerInstance.m_InRange.Add(_enemy);
    }

    public static void RemoveEnemyFromList(Enemy _enemy)
    {
        Debug.Log("Zu sp�t");
        m_PlayerInstance.m_Remove.Remove(_enemy);
    }

    public static void RemoveEmidiatlyEnemyFromList(Enemy _enemy)
    {
        Debug.Log("Zu sp�t");
        m_PlayerInstance.m_InRange.Remove(_enemy);
    }

    protected void ChangeAnimation(EPlayerState _state)
    {
        m_Animator.Play(CharacterName + "_" + _state.ToString());
    }

    private bool ChangeState(EPlayerState _state)
    {
        if(m_CurrentPlayerState != m_NextPlayerState)
        {
            return false;
        }
        //if(!Attackable)
        //{
        //    return false;
        //}
        if(_state != EPlayerState.Hurt && _state != EPlayerState.Die)
        {
            Attackable = true;
        }
        switch (_state)
        {
            case EPlayerState.Jump:
                { 
                    if(!m_GotJumpReset)
                    {
                        return false;
                    }
                }break;
            case EPlayerState.DashEntry:
                {
                    if(m_DashCooldown > 0 || !m_GotDashReset)
                    {
                        return false;
                    }
                }break;
            case EPlayerState.Attack1:
                {
                    if(m_AttackCoolDown > 0)
                    {
                        return false;
                    }
                }
                break;
        }
        PlaySound();
        m_NextPlayerState = _state;
        m_ChangeState = true;
        return true;
    }


    private void PlaySound()
    {
        m_AudioSource.Stop();
        m_AudioSource.loop = false;
        switch (PlayerState)
        {
            case EPlayerState.Run:
                {
                    m_AudioSource.loop = true;
                    m_AudioSource.clip = m_RunSound;
                }
                break;
            case EPlayerState.Jump:
                {
                    m_AudioSource.clip = m_JumpSound;
                }
                break;
            case EPlayerState.Slide:
                {
                    m_AudioSource.loop = true;
                    m_AudioSource.clip = m_SlideSound;
                }
                break;
            case EPlayerState.DashEntry:
                {
                    m_AudioSource.clip = m_DashEntrySound;
                }
                break;
            case EPlayerState.Dash:
                {
                    m_AudioSource.loop = true;
                    m_AudioSource.clip = m_DashSound;
                }
                break;
            case EPlayerState.DashExit:
                {
                    m_AudioSource.clip = m_DashExitSound;
                }
                break;
            case EPlayerState.Attack1:
                {
                    m_AudioSource.clip = m_Attack1Sound;
                }
                break;
            case EPlayerState.Attack2:
                {
                    m_AudioSource.clip = m_Attack2Sound;
                }
                break;
            case EPlayerState.Attack3:
                {
                    m_AudioSource.clip = m_Attack3Sound;
                }
                break;
            case EPlayerState.Hurt:
                {
                    m_AudioSource.clip = m_HurtSound;
                }
                break;
            case EPlayerState.Die:
                {
                    m_AudioSource.clip = m_DieSound;
                }
                break;
        }
        m_AudioSource.Play();
    }


    protected virtual void PlayerAwake()
    {
        if(m_PlayerInstance != null)
        {
            Destroy(this);
            return;
        }
        m_PlayerInstance = this;
        m_RB = GetComponent<Rigidbody2D>();
        //m_RB.AddForce(Vector2.right * PLAYER_DASH_STRENGTH, ForceMode2D.Impulse);
        m_Animator = GetComponent<Animator>();
        m_RB.velocity = Vector2.right * PLAYER_SPEED;
        m_CurrentPlayerState = m_NextPlayerState = EPlayerState.Run;
        m_CurrentBehaviour = RunBehaviour();
        m_Collider = GetComponent<BoxCollider2D>();
        m_AudioSource = GetComponent<AudioSource>();
    }

    protected virtual void PlayerUpdate()
    {
        if(transform.position.x >= FINISH_LINE)
        {
            GameSceneManagement.SwitchScene(GameSceneManagement.EGameScenes.GameWin);
        }
        m_CurrentBehaviour.MoveNext();
        if(m_CurrentBehaviour.Current)
        {
            switch (m_NextPlayerState)
            {
                case EPlayerState.Run:
                    {
                        m_CurrentBehaviour = RunBehaviour();
                    }
                    break;
                case EPlayerState.Jump:
                    {
                        m_CurrentBehaviour = JumpBehaviour();
                    }
                    break;
                case EPlayerState.Fall:
                    {
                        m_CurrentBehaviour = FallBehaviour();
                    }
                    break;
                case EPlayerState.Slide:
                    {
                        m_CurrentBehaviour = SlideBehaviour();
                    }
                    break;
                case EPlayerState.DashEntry:
                    {
                        m_CurrentBehaviour = DashEntryBehaviour();
                    }
                    break;
                case EPlayerState.Dash:
                    {
                        m_CurrentBehaviour = DashBehaviour();
                    }
                    break;
                case EPlayerState.DashExit:
                    {
                        m_CurrentBehaviour = DashExitBehaviour();
                    }
                    break;
                case EPlayerState.Attack1:
                    {
                        m_CurrentBehaviour = Attack1Behaviour();
                    }
                    break;
                case EPlayerState.Attack2:
                    {
                        m_CurrentBehaviour = Attack2Behaviour();
                    }
                    break;
                case EPlayerState.Attack3:
                    {
                        m_CurrentBehaviour = Attack3Behaviour();
                    }
                    break;
                case EPlayerState.Die:
                    {
                        m_CurrentBehaviour = DieBehaviour();
                    }break;
                case EPlayerState.Hurt:
                    {
                        m_CurrentBehaviour = HurtBehaviour();
                    }break;
                default:
                    break;
            }
        }
        m_JumpKeyDown = false;
        m_SlideKeyDown = false;
        m_DashKeyDown = false;
        m_AttackKeyDown = false;
    }

    private void HoldSpeed()
    {
        if (m_RB.velocity.x < PLAYER_SPEED)
        {
            m_VelocityChanger.x = PLAYER_ACCELERATION * Time.fixedDeltaTime;
            m_VelocityChanger.y = 0;
            m_RB.AddForce(m_VelocityChanger, ForceMode2D.Force);
        }
    }

    private void PlayerFixedUpdate()
    {
        if(m_CurrentPlayerState == EPlayerState.DashEntry || m_CurrentPlayerState == EPlayerState.Dash || m_CurrentPlayerState == EPlayerState.DashExit)
        {
            transform.position += m_DashDirection * PLAYER_DASH_SPEED * Time.fixedDeltaTime;
        }
        m_AttackCooldownDelay = Mathf.Clamp(m_AttackCooldownDelay - Time.fixedDeltaTime, 0, PLAYER_ATTACK_COOLDOWN_DELAY);
        Debug.Log("Cooldown: " + m_AttackCooldownDelay);
        m_AttackCoolDown = Mathf.Clamp(m_AttackCoolDown - Time.fixedDeltaTime, 0, PLAYER_ATTACK_COOLDOWN);
        m_DashCooldown = Mathf.Clamp(m_DashCooldown - Time.fixedDeltaTime, 0, PLAYER_DASH_COOLDOWN);
        m_LeftDashTime = Mathf.Clamp(m_LeftDashTime - Time.fixedDeltaTime, 0, PLAYER_DASH_DURATION);
        m_DashEntryTime = Mathf.Clamp(m_DashEntryTime - Time.fixedDeltaTime, 0, PLAYER_DASH_ENTRY);
        m_DashExitTime = Mathf.Clamp(m_DashExitTime - Time.fixedDeltaTime, 0, PLAYER_DASH_EXIT);
    }

    protected void DashFullReset()
    {
        m_GotDashReset = true;
        m_DashCooldown = 0f;
    }

    protected bool Attack()
    {
        if(m_InRange.Count <= 0)
        {
            return false;
        }
        for (int i = 0; i < m_InRange.Count; i++)
        {
            m_InRange[i].GetDamage(1);
            GameRegistry.m_TimeFreez = true;      
        }
        m_AttackStack++;
        
        if(m_AttackStack >= PLAYER_ATTACK_AMOUNT)
        {
            m_AttackCoolDown = PLAYER_ATTACK_COOLDOWN;
            DashFullReset();
            GameRegistry.m_TimeFreez = false;
        }
        else
        {
        }
        return true;
    }

    private void NockAllBack()
    {
        for (int i = 0; i < m_InRange.Count; i++)
        {
            m_InRange[i].LargeNockback();
        }
        m_InRange.Clear();
    }

    #endregion Methodes
}
