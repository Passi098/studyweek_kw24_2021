using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class GameSceneManagement
{
    public enum EGameScenes { MainMenu, Options, Credits, MainGame, GameWin, GameLost }

    public static void SwitchScene(EGameScenes _scene)
    {
        switch (_scene)
        {
            case EGameScenes.MainMenu:
                break;
            case EGameScenes.Options:
                break;
            case EGameScenes.Credits:
                break;
            case EGameScenes.MainGame:
                {
                    GameRegistry.ResetGameData();
                }break;
            case EGameScenes.GameWin:
                break;
            case EGameScenes.GameLost:
                break;
        }

        SceneManager.LoadScene((int)_scene);

        switch (_scene)
        {
            case EGameScenes.MainMenu:
                break;
            case EGameScenes.Options:
                break;
            case EGameScenes.Credits:
                break;
            case EGameScenes.MainGame:
                break;
            case EGameScenes.GameWin:
                break;
            case EGameScenes.GameLost:
                break;
        }
    }
}
