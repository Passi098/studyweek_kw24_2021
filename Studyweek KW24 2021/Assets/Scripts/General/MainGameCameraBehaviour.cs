using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainGameCameraBehaviour : MonoBehaviour
{
    void Update()
    {
        transform.position = new Vector3(Mathf.Clamp(Player.PlayerPosition.x + 7,0, 311.5f), Mathf.Clamp(Player.PlayerPosition.y, 0, 10000), -10);
    }
}
