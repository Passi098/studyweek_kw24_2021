using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameRegistry
{
    private const int TIMER_START_VALUE = 60;
    private const int PLAYER_MAX_HEALTH = 3;

    private static float m_Timer = TIMER_START_VALUE;
    public static bool m_TimeFreez = false;
    private static int m_Health = PLAYER_MAX_HEALTH;

    public static float Timer
    {
        get { return m_Timer; }
        set 
        { 
            m_Timer = value;
            MainGameUI.UpdateTimerUI();
            if(m_Timer <= 0)
            {
                CheckGameLost();
            }
        }
    }

    public static int PlayerHealth
    {
        get { return m_Health; }
        set 
        { 
            m_Health = value; 
        }
    }

    public static void CheckGameLost()
    {
        if(m_Timer <= 0)
        {
            GameSceneManagement.SwitchScene(GameSceneManagement.EGameScenes.GameLost);
        }
    }

    public static void CheckGameWin()
    {

    }

    public static void ResetGameData()
    {
        m_Timer = TIMER_START_VALUE;
        m_Health = PLAYER_MAX_HEALTH;
        Player.Attackable = true;
    }
}
