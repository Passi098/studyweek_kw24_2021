using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background3 : MonoBehaviour
{
    private static float m_Start = 0;
    private static float m_End = 350;

    [SerializeField] private float speed;

    private void FixedUpdate()
    {
        transform.position = new Vector3(Mathf.Lerp(m_Start, m_End, Camera.main.transform.position.x / m_End) * speed, -4, 0);
    }
}
